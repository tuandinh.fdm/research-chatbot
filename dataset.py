import csv
import json

class DataSet:
    def __init__(self, name=None,
                        k_shot=-1):
        if name not in ['banking', 'clinc', 'hwu', 'vifin']:
            print('Error dataset name')
            raise
        if k_shot == -1:
            file_name = 'train.csv'
        elif k_shot == 5:
            file_name = 'train_5.csv'
        elif k_shot == 10:
            file_name = 'train_10.csv'
        else:
            file_name = 'train.csv'
        self.train_path = 'data' + '/' + name + '/' + file_name
        self.test_path = 'data' + '/' + name + '/test.csv'
        self.list_categories_path = 'data' + '/' + name + '/categories.json'
        self.train_texts = []
        self.train_labels = []
        self.test_texts = []
        self.test_labels = []
        self.list_categories = []

    def read(self):
        with open(self.train_path, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            headers = next(reader, None)
            for row in reader:
                self.train_texts.append(row[0])
                self.train_labels.append(row[1])
        with open(self.test_path, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            headers = next(reader, None)
            for row in reader:
                self.test_texts.append(row[0])
                self.test_labels.append(row[1])
        self.list_categories = json.load(open(self.list_categories_path))


class DataSetLoader:
    def __init__(self,
                 batch_size=32,
                 max_len=128,
                 tokenizer=None):
        self.batch_size = batch_size
        self.max_len = max_len
        self.tokenizer = tokenizer

    def loader(self, dataset: DataSet):
        pass
