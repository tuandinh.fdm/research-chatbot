# Research Chatbot

## Datasets


| Dataset      	| Size 	| Description                                                                         	| License                    	|
|--------------	|------	|-------------------------------------------------------------------------------------	|----------------------------	|
| [Banking77](https://arxiv.org/abs/2003.04807)      	| 13K  	| online banking queries                                                              	| CC-BY-4.0                  	|
| [HWU64](https://arxiv.org/abs/1903.05566)          	| 11K  	| popular personal assistant queries                                                  	| CC-BY-SA 3.0               	|
| [CLINC150](https://www.aclweb.org/anthology/D19-1131/)        	| 20K  	| popular personal assistant queries                                                  	| CC-BY-SA 3.0               	|
| VIFIN16        	| 11K  	| popular personal assistant queries                                                  	|              	|

## Training
**Banking77**
```
python train.py \
        --name_dataset banking \
        --k_shot -1 \
        --pretrained bert-large-cased \
        --enable_transformer_encoder True \
        --freeze_bert True --cls cls --num_epochs 100 --learning_rate 4e-4 \
        --batch_size 16 \
```
**CLINC150**

```
python train.py \
        --name_dataset clinc \
        --k_shot -1 \
        --pretrained bert-large-cased \
        --enable_transformer_encoder True \
        --freeze_bert True --cls cls --num_epochs 100 --learning_rate 4e-4 \
        --batch_size 16 
```

**HWU64**

```
python train.py \
        --name_dataset hwu \
        --k_shot -1 \
        --pretrained bert-large-cased \
        --enable_transformer_encoder True \
        --freeze_bert True --cls cls --num_epochs 100 --learning_rate 4e-4 \
        --batch_size 16 
```

**VIFIN16**
```
python train.py \
        --name_dataset vifin \
        --k_shot -1 \
        --pretrained bert-large-cased \
        --enable_transformer_encoder True \
        --freeze_bert True --cls cls --num_epochs 100 --learning_rate 4e-4 \
        --batch_size 16 
```
