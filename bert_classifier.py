import torch
from torch import nn
from transformers import BertModel

class BertClassifier(nn.Module):
    """Bert Model for Classification Tasks.
    """

    def __init__(self,
                 num_class=None,
                 pretrain='bert-base-cased',
                 hidden_layer_dim=512,
                 vocab_size=None,
                 max_len=128,
                 cls='mean',
                 drop_out=0.5,
                 freeze_bert=False,
                 enable_transformer_encoder=False,
                 activation="relu",
                 nhead=8,
                 dim_feedforward=2048,
                 num_encoder_layers=2):
        """
        @param    bert: a BertModel object
        @param    classifier: a torch.nn.Module classifier
        @param    freeze_bert (bool): Set `False` to fine-tune the BERT model
        """
        super(BertClassifier, self).__init__()
        self.num_class = num_class
        self.cls = cls
        self.enable_transformer_encoder = enable_transformer_encoder
        self.max_len = max_len
        # Specify hidden size of BERT, hidden size of our classifier, and number of labels

        # Instantiate BERT model
        self.bert = BertModel.from_pretrained(pretrain)

        D_in, H, D_out = self.bert.config.hidden_size, hidden_layer_dim, self.num_class

        if self.enable_transformer_encoder:
            self.embedding = nn.Embedding(vocab_size, D_in)
            self.position_embedding = nn.Embedding(self.max_len, D_in)
            self.feed_forward = nn.Linear(D_in * 2, H)
            self.transformer_encoder = nn.TransformerEncoder(
                nn.TransformerEncoderLayer(d_model=H, nhead=nhead, dim_feedforward=dim_feedforward, dropout=drop_out,
                                           activation=activation),
                num_encoder_layers,
                # LayerNorm(d_model)
            )
        # Instantiate an one-layer feed-forward classifier
        self.classifier = nn.Sequential(
            nn.Linear(H, D_out)
        )
        self.loss_fn = nn.CrossEntropyLoss()
        # Freeze the BERT model
        if freeze_bert:
            for param in self.bert.parameters():
                param.requires_grad = False

    def forward(self, input_ids, attention_mask):

        # Feed input to BERT
        outputs = self.bert(input_ids=input_ids,
                            attention_mask=attention_mask)
        if self.enable_transformer_encoder:
            embedding = self.embedding(input_ids)
            embedding += self.position_embedding(
                torch.arange(self.max_len).repeat(input_ids.size(0), 1).type_as(input_ids)
            )
            embedding = torch.cat((outputs[0], embedding), dim=2)
            embedding = self.feed_forward(embedding)
            outputs = self.transformer_encoder(embedding)
        # Extract the last hidden state of the token `[CLS]` for classification task
        if self.cls == 'cls':
            last_hidden_state_cls = outputs[0][:, 0, :]
        elif self.cls == 'mean':
            last_hidden_state_cls = torch.mean(outputs[0], 1)
        else:
            last_hidden_state_cls = outputs[0][:, 0, :]
        # Feed input to classifier to compute logits
        logits = self.classifier(last_hidden_state_cls)
        return logits



