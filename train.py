from utils import *
from bert_classifier import BertClassifier
from dataset import DataSetLoader, DataSet
import argparse
import random
import numpy as np
import torch
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from transformers import BertTokenizer
from transformers import AdamW
from transformers import get_linear_schedule_with_warmup
from tqdm import tqdm


# num_class = None,
# pretrain = 'bert-base-cased',
# hidden_layer_dim = 512,
# vocab_size = None,
# max_len = 128,
# cls = 'mean',
# drop_out = 0.5,
# freeze_bert = False,
# enable_transformer_encoder = False,
# activation = "relu",
# nhead = 8,
# dim_feedforward = 2048,
# num_encoder_layers = 2
def read_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--name_dataset", type=str, default="banking")
    parser.add_argument("--k_shot", type=int, default=-1)
    parser.add_argument("--pretrained", type=str, default='bert-base-cased')
    parser.add_argument("--enable_transformer_encoder", type=bool, default=False)
    parser.add_argument("--freeze_bert", type=bool, default=True)
    parser.add_argument("--cls", type=str, default='cls')
    parser.add_argument("--dropout", type=float, default=0.5)
    parser.add_argument("--learning_rate", default=5e-5, type=float)
    parser.add_argument("--adam_epsilon", default=1e-8, type=float)
    parser.add_argument("--device", default=0, type=int)
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--batch_size", type=int, default=16)
    parser.add_argument("--max_len", type=int, default=128)
    parser.add_argument("--epochs", type=int, default=50)
    return  parser.parse_args()
def train(args):
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)

    if torch.cuda.is_available():
        device = torch.device("cuda:" + str(args.device))
        print(f'There are {torch.cuda.device_count()} GPU(s) available.')
        print('Device name:', torch.cuda.get_device_name(0))

    else:
        print('No GPU available, using the CPU instead.')
        device = torch.device("cpu")

    # load dataset
    dataset = DataSet(name=args.name_dataset, k_shot=args.k_shot)
    dataset.read()
    train_texts, train_labels, test_texts, test_labels, list_categories = \
        dataset.train_texts, dataset.train_labels, dataset.test_texts, dataset.test_labels, dataset.list_categories

    # processing data
    tokenizer = BertTokenizer.from_pretrained(args.pretrained)

    train_texts = [text_preprocessing(text) for text in train_texts]
    test_texts = [text_preprocessing(text) for text in test_texts]
    train_inputs, train_masks = preprocessing_for_bert(data=train_texts, tokenizer=tokenizer, max_len=args.max_len)
    test_inputs, test_masks = preprocessing_for_bert(data=test_texts, tokenizer=tokenizer, max_len=args.max_len)

    train_labels = [label2id(label, list_categories=list_categories) for label in train_labels]
    test_labels = [label2id(label, list_categories=list_categories) for label in test_labels]
    # Convert other data types to torch.Tensor
    train_labels = torch.tensor(train_labels)
    test_labels = torch.tensor(test_labels)

    print(train_inputs.shape)
    print(test_inputs.shape)
    print(train_labels.shape)
    print(test_labels.shape)

    # Create the DataLoader for our training set
    train_data = TensorDataset(train_inputs, train_masks, train_labels)
    train_sampler = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=args.batch_size)

    # Create the DataLoader for our validation set
    test_data = TensorDataset(test_inputs, test_masks, test_labels)
    test_sampler = RandomSampler(test_data)
    test_dataloader = DataLoader(test_data, sampler=test_sampler, batch_size=args.batch_size)

    model = BertClassifier(
        num_class=len(list_categories),
        pretrain=args.pretrained,
        vocab_size=tokenizer.vocab_size,
        cls=args.cls,
        freeze_bert=args.freeze_bert,
        enable_transformer_encoder=args.enable_transformer_encoder,
    )
    model.to(args.device)
    optimizer = AdamW(model.parameters(),
                      lr=args.learning_rate,  # args.learning_rate - default is 5e-5, our notebook had 2e-5
                      eps=args.adam_epsilon  # args.adam_epsilon  - default is 1e-8.
                      )
    epochs = args.epochs
    scheduler = get_linear_schedule_with_warmup(optimizer,
                                                num_warmup_steps=0,  # Default value in run_glue.py
                                                num_training_steps=len(train_dataloader) * epochs)
    loss_values = []

    for epoch_i in range(0, epochs):
        print("")
        print('======== Epoch {:} / {:} ========'.format(epoch_i + 1, epochs))
        print('Training...')
        model.train()
        total_loss = 0
        # For each batch of training data...
        for step, batch in enumerate(tqdm(train_dataloader)):
            b_input_ids = batch[0].to(device)
            b_input_mask = batch[1].to(device)
            b_labels = batch[2].to(device)
            # model.zero_grad()
            optimizer.zero_grad()
            logits = model(b_input_ids, attention_mask=b_input_mask)
            loss = model.loss_fn(logits, b_labels)
            total_loss += loss.item()
            loss.backward()
            # torch.nn.utils.clip_grad_norm_(model.classifier.parameters(), 1.0)
            optimizer.step()
            scheduler.step()
        avg_train_loss = total_loss / len(train_dataloader)

        loss_values.append(avg_train_loss)
        print('Loss values: ')
        print(np.mean(np.array(loss_values)))
        print("")
        print("Running Validation...")

        model.eval()
        # Tracking variables
        eval_loss, eval_accuracy = 0, 0
        nb_eval_steps, nb_eval_examples = 0, 0
        # Evaluate data for one epoch
        for batch in test_dataloader:
            # Add batch to GPU
            batch = tuple(t.to(device) for t in batch)

            # Unpack the inputs from our dataloader
            b_input_ids, b_input_mask, b_labels = batch
            with torch.no_grad():
                outputs = model(b_input_ids,
                                attention_mask=b_input_mask,
                                )
            logits = outputs
            # Move labels to CPU
            label_ids = b_labels.to('cpu').numpy()
            preds = torch.argmax(logits, dim=1)
            preds = preds.detach().cpu().numpy()

            tmp_eval_accuracy = flat_accuracy(preds, label_ids)

            eval_accuracy += tmp_eval_accuracy
            nb_eval_steps += 1
        # Report the final accuracy for this validation run.
        print("  Accuracy: {0:.4f}".format(eval_accuracy / nb_eval_steps))


if __name__ == "__main__":
    args = read_args()
    print(args)
    train(args)

